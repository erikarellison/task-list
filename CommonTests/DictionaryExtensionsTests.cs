﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using Common.Extensions;

namespace TaskListLogicTests
{
    [TestFixture]
    public class DictionaryExtensionsTests
    {   
        [Test]
        [TestCaseSource("ShouldGetValueOrDefaultTestCasesPrimitive")]
        public void Should_Get_Value_Or_Default_Primitive(Dictionary<int, int> dictionary, int key, int expectedResult)
        {
            dictionary.GetValueOrDefault(key).Should().Be(expectedResult);
        }

        [Test]
        [TestCaseSource("ShouldGetValueOrDefaultTestCasesObject")]
        public void Should_Get_Value_Or_Default_Object(Dictionary<int, string> dictionary, int key, string expectedResult)
        {
            dictionary.GetValueOrDefault(key).Should().Be(expectedResult);
        }

        private IEnumerable<TestCaseData> ShouldGetValueOrDefaultTestCasesPrimitive()
        {
            yield return new TestCaseData(new Dictionary<int, int>(), 2, default(int))
                .SetName("ShouldGetValueOrDefault primitive default");

            yield return new TestCaseData(new Dictionary<int, int>{ { 2, 8 } }, 2, 8)
                .SetName("ShouldGetValueOrDefault primitive value");
        }

        private IEnumerable<TestCaseData> ShouldGetValueOrDefaultTestCasesObject()
        {
            yield return new TestCaseData(new Dictionary<int, string> { }, 2, default(string))
                .SetName("ShouldGetValueOrDefault object default");

            yield return new TestCaseData(new Dictionary<int, string> { { 2, "lorem ipsum" } }, 2, "lorem ipsum")
                .SetName("ShouldGetValueOrDefault object value");
        } 

    }
}