﻿using System;

namespace TaskListLogic.Models
{
    public class TaskSummary
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Overdue { get; set; }
    }
}