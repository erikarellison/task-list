﻿using System;

namespace TaskListLogic.Models
{
    public class TaskItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? Deadline { get; set; }
        public bool Completed { get; set; }
        public string Details { get; set; }
    }
}