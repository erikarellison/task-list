﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskListLogic.Models;

namespace TaskListLogic.Logic
{
    public interface ITaskLogic
    {
        Task<IEnumerable<TaskSummary>> ListUserTasks(Guid userId);

        Task<TaskItem> GetUserTask(Guid userId, Guid taskId);

        Task<Guid> SaveUserTask(Guid userId, TaskItem task);

        Task DeleteUserTask(Guid userId, Guid taskId);
    }
}