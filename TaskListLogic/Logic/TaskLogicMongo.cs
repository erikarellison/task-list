﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using TaskListLogic.Models;

namespace TaskListLogic.Logic
{
    public class TaskLogicMongo : ITaskLogic
    {
        public async Task<IEnumerable<TaskSummary>> ListUserTasks(Guid userId)
        {
            var collection = GetTaskTable();

            var docs = await collection.Find(new BsonDocument()).ToListAsync();

            var summaries = docs.Select(x => BsonSerializer.Deserialize<TaskItem>(x)).Select(x => new TaskSummary
            {
                Id = x.Id,
                Name = x.Name,
                Overdue = x.Deadline.HasValue && x.Deadline.Value < DateTime.UtcNow
            });

            return summaries;
        }

        public Task<TaskItem> GetUserTask(Guid userId, Guid taskId)
        {
            throw new NotImplementedException();
        }

        public async Task<Guid> SaveUserTask(Guid userId, TaskItem task)
        {
            if (task.Id == Guid.Empty)
                task.Id = Guid.NewGuid();

            var collection = GetTaskTable();

            var doc = task.ToBsonDocument();

            await collection.InsertOneAsync(doc);

            return task.Id;
        }

        public Task DeleteUserTask(Guid userId, Guid taskId)
        {
            throw new NotImplementedException();
        }

        private IMongoCollection<BsonDocument> GetTaskTable()
        {
            var client = new MongoClient(MongoConnectionString);

            var db = client.GetDatabase("task-list");

            var collection = db.GetCollection<BsonDocument>("task");

            return collection;
        }

        private const string MongoConnectionString = "mongodb://localhost";
    }
}