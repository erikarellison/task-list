TASK LIST
=========


Overview
--------
This is a simplied implementation on of an application 
to allow users to manage their Task List (to do list).

So that you can reference the requirements I worked off of, the options I chose were

* Domain: To-Do List
* Interface: Web API
* Platform: .NET
* Language: C#

As suggested, the tasks are only persisted in memory, 
and in the interest of time (and also because the acceptance criteria didn't mention it),
there is no authentication and only a hasty and hacky approach to supporting multiple users 
(hope you have cookies enabled, or you're boned! =D).

The project organization feels like overkill for the size/scope of the project,
but I did it anyway so it look closer to how I'd organize a 'real-life' solution.

One note, the acceptance criteria asked to 'highlight' items whose deadlines have passed,
which I've done with an overdue flag on the summary items when getting the task list.
This logic assumes datetimes are submitted to the API and persisted in UTC.

One more note, the acceptance criteria said "As a user, I can mark a TODO as completed.",
which I addressed by having the API support updating a TODO, which includes setting the Completed flag.
I could also have added an endpoint for just setting complete - PUT/PATCH tasks/{id}/complete,
but it seems like the update funcionality would be desired anyway, and TODO objects will likely be
small enough that the performance impact of updating the whole object instead of one property
will probably not be large enough to register.


Running
-------
Since VisualStudio was suggested as an IDE, and this is for a .NET position, I'm assuming you have it.
This was written in 2015, but it should run fine in 2013 as the acceptance criteria recommends.

The easiest thing would be to open TaskList.sln with VS, set TaskListApi as the start up project, and run from VS.
You can hit the endpoints below (routes also doc'd with comments in TasksController.cs)
with Postman or some other HTTP request client, make sure to set a header for Content-Type: application/json

* GET api/tasks
* POST api/tasks
	[example request body:
	{
	  "name": "Shower",
	  "deadline": "8/20/2015 8:00AM"
	  "completed": false,
	  "details": ""
	}]
* GET api/tasks/{id}
* PUT api/tasks/{id} 
	[example request body:
	{
	  "name": "Shower",
	  "deadline": "8/20/2015 8:00AM",
	  "completed": true,
	  "details": "make sure to wash behind ears"
	}]
* DELETE api/tasks/{id}

The easiest way to run the unit tests is to have or get the NUnit test adapter extension in VS,
the tests will be discovered on build and you can run them via Test -> Windows -> Test Explorer.


Third Party Libraries
---------------------
It said to mention this, so just a heads up that I used:
	SimpleInjector for DI (not that I went crazy with this)
	NUnit and FluentAssertions for readable unit tests


Contact
-------
If you have any questions, please feel free to contact me. I have included
my cell number for completeness, but really email is the best way to reach me.
erika.r.ellison@gmail.com
360-224-4438