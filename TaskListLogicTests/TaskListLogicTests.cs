﻿using System;
using System.Linq;
using Common.Exceptions;
using FluentAssertions;
using NUnit.Framework;
using TaskListLogic.Logic;
using TaskListLogic.Models;

namespace TaskListLogicTests
{
    /// <summary>
    /// These are weird unit tests, really more like integration tests. Ideally unit tests isolate just one method, 
    /// not test both at the same time or the results of one given having called the other, but
    /// given the logic encapsulates its singleton implementation, I didn't see a quick and easy way to do that, so here we are.
    /// </summary>
    [TestFixture]
    public class TaskListLogicTests
    {
        [SetUp]
        public void SetUp()
        {
            this.userId = Guid.NewGuid();
            this.logic = new TaskLogicMongo();
        }

        [Test]
        public async void Should_List_User_Tasks()
        {
            (await logic.ListUserTasks(userId)).Count().Should().Be(0);

            logic.SaveUserTask(userId, new TaskItem { Name = "first", Deadline = DateTime.UtcNow.AddDays(-2) });
            logic.SaveUserTask(userId, new TaskItem { Name = "second", Deadline = DateTime.UtcNow.AddDays(2) });

            var tasks = await logic.ListUserTasks(userId);

            tasks.Count().Should().Be(2);

            var task = tasks.SingleOrDefault(x => x.Name == "first");
            task.Should().NotBeNull();
            task.Overdue.Should().BeTrue();

            task = tasks.SingleOrDefault(x => x.Name == "second");
            task.Should().NotBeNull();
            task.Overdue.Should().BeFalse();
        }

        [Test]
        public async void Should_Save_And_Get_User_Task()
        {
            var deadline = DateTime.UtcNow;

            var taskId = await logic.SaveUserTask(userId, new TaskItem
            {
                Name = "test task",
                Deadline = deadline,
                Completed = true,
                Details = "test details" 
            });

            var task = await logic.GetUserTask(userId, taskId);

            task.Id.Should().Be(taskId);
            task.Name.Should().Be("test task");
            task.Deadline.Should().Be(deadline);
            task.Completed.Should().Be(true);
            task.Details.Should().Be("test details");
        }

        [Test]
        public async void Should_Delete_User_Task()
        {
            (await logic.ListUserTasks(userId)).Count().Should().Be(0);

            var taskId = await logic.SaveUserTask(userId, new TaskItem { Name = "test task" });
            (await logic.ListUserTasks(userId)).Count().Should().Be(1);

            logic.DeleteUserTask(userId, taskId);

            (await logic.ListUserTasks(userId)).Count().Should().Be(0);

            Action action = () => { logic.GetUserTask(userId, taskId); };
            action.ShouldThrow<NotFoundException>();

            action = () => { logic.DeleteUserTask(userId, taskId); };
            action.ShouldThrow<NotFoundException>();
        }

        private Guid userId;
        private ITaskLogic logic;
    }
}