﻿using System.Collections.Generic;

namespace Common.Extensions
{
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Convenience wrapper around the TryGet method. This returns default(T) if the key is not in the dictionary, otherwise returns the value.
        /// </summary>
        public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) 
        {
            TValue value;
            if (dictionary.TryGetValue(key, out value))
                return value;
            return default(TValue);
        }
    }
}