﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Common.Extensions;

namespace TaskListApi
{
    public static class CookieHelper
    {
        public static Guid? GetSessionId(HttpRequestMessage requestMessage)
        {
            var cookie = requestMessage.Headers.GetCookies(CookieName).FirstOrDefault();
            // if the request doesn't have a cookie, SetSessionId will have put the session id in the properties bag
            var sessionIdString = (cookie != null)
                ? cookie[CookieName].Value
                : requestMessage.Properties.GetValueOrDefault(CookieName) as string;

            Guid sessionId;
            return (Guid.TryParse(sessionIdString, out sessionId)) ? sessionId : (Guid?)null;
        }

        public static void SetSessionIdRequest(HttpActionContext actionContext)
        {
            var sessionId = Guid.NewGuid().ToString();
            actionContext.Request.Properties[CookieName] = sessionId;
        }

        public static void SetSessionIdResponse(HttpActionExecutedContext actionExecutedContext)
        {
            var sessionId = GetSessionId(actionExecutedContext.Request);
            var userCookie = new CookieHeaderValue(CookieName, sessionId.ToString());
            // the response will be null if the action threw an exception
            if (actionExecutedContext.Response != null)
                actionExecutedContext.Response.Headers.AddCookies(new[] { userCookie });
        }

        private const string CookieName = "TaskListApiSessionId";
    }
}