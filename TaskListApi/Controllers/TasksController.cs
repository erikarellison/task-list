﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using TaskListLogic.Logic;
using TaskListLogic.Models;

namespace TaskListApi.Controllers
{
    public class TasksController : ApiController
    {
        public TasksController(ITaskLogic taskLogic)
        {
            if (taskLogic == null)
                throw new ArgumentNullException("taskLogic");

            this.taskLogic = taskLogic;
        }

        // GET api/tasks
        public async Task<IEnumerable<TaskSummary>> Get()
        {
            return await taskLogic.ListUserTasks(UserId);
        }

        // GET api/tasks/{id}
        public async Task<TaskItem> Get(Guid id)
        {
            return await taskLogic.GetUserTask(UserId, id);
        }

        // POST api/tasks
        public async Task<Guid> Post([FromBody]TaskItem task)
        {
            return await taskLogic.SaveUserTask(UserId, task);
        }

        // PUT api/tasks/5
        public async Task Put(Guid id, [FromBody]TaskItem task)
        {
            await taskLogic.SaveUserTask(UserId, task);
        }

        // DELETE api/tasks/5
        public async Task Delete(Guid id)
        {
            await taskLogic.DeleteUserTask(UserId, id);
        }

        // normally user context would go through authentication, and be dependency injected also,
        // but I didn't want to get too crazy with user context stuff, multi-user support may not even be strictly required in the first place
        // but it made the logic look a little more real-life I guess
        private Guid UserId
        {
            get
            {
                var sessionId = CookieHelper.GetSessionId(this.ControllerContext.Request);
                if (sessionId == null)
                    throw new Exception("Something has gone horribly wrong!");

                return sessionId.Value;
            }
        }

        private ITaskLogic taskLogic;
    }
}
