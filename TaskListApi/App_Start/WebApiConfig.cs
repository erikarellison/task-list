﻿using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using TaskListLogic.Logic;

namespace TaskListApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // global exception filter
            config.Filters.Add(new UnhandledExceptionFilter());

            // global action filter to manage user context via cookie
            config.Filters.Add(new UserCookieActionFilter());

            // configure DI
            ConfigureDependencyInjection(config);

            // default to JSON api responses
            ConfigureJsonSerialization(config);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void ConfigureDependencyInjection(HttpConfiguration config)
        {
            var container = new Container();

            container.RegisterWebApiRequest<ITaskLogic, TaskLogicMongo>();

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            config.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }

        private static void ConfigureJsonSerialization(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            var jsonFormatter = config.Formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented; 
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
