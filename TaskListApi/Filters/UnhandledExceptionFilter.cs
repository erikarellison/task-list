﻿using Common.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace TaskListApi
{
    internal class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            Exception exception = actionExecutedContext.Exception;
            var responseCode = MapExceptionTypeToHttpStatusCode(exception);
            var response = actionExecutedContext.Request.CreateErrorResponse(responseCode, exception.Message, exception);
            actionExecutedContext.Response = response;
        }

        private static HttpStatusCode MapExceptionTypeToHttpStatusCode(Exception exception)
        {
            if (exception is NotFoundException)
                return HttpStatusCode.NotFound;
            if (exception is ValidationException)
                return HttpStatusCode.BadRequest;

            return HttpStatusCode.InternalServerError;
        }
    }
}