﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace TaskListApi
{
    public class UserCookieActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var sessionId = CookieHelper.GetSessionId(actionContext.Request);
            if (sessionId == null)
                CookieHelper.SetSessionIdRequest(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            CookieHelper.SetSessionIdResponse(actionExecutedContext);
        }
    }
}